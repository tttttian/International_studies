'use strict';
const path = require('path');
// had enabled by egg
exports.static = true;

exports.cluster = {
    enable: true,
    package: 'egg-cluster'
};

exports.sequelize = {
    enable: true,
    package: 'egg-sequelize',
};

exports.validate = {
    enable: true,
    package: 'egg-validate'
};

exports.session = true;

exports.logview = {
    package: 'egg-logview',
};


exports.knex = {
    enable: true,
    package: 'egg-knex',
};

// exports.cors = {
//     enable: true,
//     package: 'egg-cors',
// };

exports.decoratorRouter = {
    enable: true,
    path: path.join(__dirname, '../plugin/egg-decorator-router')
};

exports.sequelizeTransactional = {
    enable: true,
    path: path.join(__dirname, '../plugin/egg-sequelize-transactional')
};

exports.controllerExceptionHandler = {
    enable: true,
    path: path.join(__dirname, '../plugin/egg-controller-exception-handler')
};

exports.multipart = true;

//egg-sequelize-auto -o "./app/model/" -d yhps -h 182.92.96.50 -u dmt18 -x dmt18@123 -p 3310 -C camel -t town community subdistrict
