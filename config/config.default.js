'use strict';
const path = require('path');
const Sequelize = require('sequelize');
const cls = require('continuation-local-storage');
const os = require('os');

module.exports = appInfo => {
    const config = {};

    config.static = {
        dir:path.join(appInfo.baseDir,'/app/public'),
        dynamic:true,
        preload:false
    };

    config.multipart = {
        mode: 'file',
        tmpdir: path.join(os.tmpdir(), 'egg-multipart-tmp', appInfo.name),
        cleanSchedule: {
            // run tmpdir clean job on every day 04:30 am
            // cron style see https://github.com/eggjs/egg-schedule#cron-style-scheduling
            cron: '0 30 4 * * *',
        },
        whitelist: [
            '.jpg', '.jpeg',
            '.png',
            '.mp3',   //假设音频格式是mp3
            '.mp4',   //假设视频格式是MP4
            '.MOV',
            '.swf',
        ],
        fileSize: 1048576000,   //视频大小，图片为 fields
    };

    config.upload = {
        dir: `${appInfo.baseDir}/app/public/upload`,
        host: 'localhost:8889'
    };

    config.middleware = ['cors'];

    // config.cors = {
    //     origin:'*',
    //     allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH'
    // };



    // config.cluster = {
    //     listen: {
    //         port: 8880,
    //     }
    // };
    config.decoratorRouter = {};


    config.session = {
        key: 'sid',
        maxAge: 'session', // 会话
        encrypt: true,
        renew: true
    };

    const namespace = cls.createNamespace('my-very-own-namespace');
    Sequelize.useCLS(namespace);
    config.namespace = namespace;
    const dbConfig = {
        dialect: 'mysql',
        database: 'intangible',
        username: 'dmt',
        password: '123456',
        host: 'localhost',
        port: '3306',
        pool: {
            max: 10, //must be greater than 1
            idle: 300000,
            acquire: 600000
        },
        define: {
            timestamps: false
        },
        Sequelize: Sequelize,
        timezone: '+08:00',
    };

    config.crypto_key = 'ea15987dfe12eed6';


    config.bodyParser = {
        jsonLimit: '50mb'
    };

    exports.logview = {};

    config.validate = {
        convert: true,
        validateRoot: false,
    };

    config.keys = appInfo.name + '_1526343653947_0101';

    config.logger = {
        appLogName: `${appInfo.name}-web.log`,
        coreLogName: 'egg-web.log',
        agentLogName: 'egg-agent.log',
        errorLogName: 'common-error.log',
    };

    config.sequelize = dbConfig;

    config.knex = {
        // database configuration
        client: {
            // database dialect
            dialect: dbConfig.dialect,
            connection: {
                // host
                host: dbConfig.host,
                // port
                port: dbConfig.port,
                // username
                user: dbConfig.username,
                // password
                password: dbConfig.password,
                // database
                database: dbConfig.database,
                requestTimeout: 600000,
            },
            // connection pool
            pool: {min: 0, max: 1},
            // acquire connection timeout, millisecond
            acquireConnectionTimeout: 600000,
        },
        // load into app, default is open
        app: true,
        // load into agent, default is close
        agent: false,
    };

    config.appBaseDir = path.join(appInfo.baseDir, 'app');

    config.development = {
        overrideDefault: true,
        watchDirs: [
            'app/controller',
            'app/service',
            'app/middleware',
            'app/model',
            'app/router',
        ]
    };

    config.security = {
        csrf: false,
    };



    return config;
};
