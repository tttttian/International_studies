const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/course')
class CourseCtrl extends Controller{

    //课程上方返回背景图
    @HttpGet('/get/bgimg')
    @ExceptionHandler()
    async getCourseBgi() {
        const rule = {
            classify:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.home.main.getBgimg();
    }

    //获取课程信息和原始图
    @HttpGet('/get/course')
    @ExceptionHandler()
    async getOriginalCourse(){
        this.result.data = await this.service.china.pc.course.main.getOriginalCourse();
    }

    //获取播放视频
    @HttpGet('/get/video')
    @ExceptionHandler()
    async getVideo(){
        const rule = {
            course_id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.getVideo(this.ctx.request.query.course_id);

    }

    //中文获取播放视频
    @HttpGet('/get/videochn')
    @ExceptionHandler()
    async getVideochn(){
        const rule = {
            course_id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.getVideochn(this.ctx.request.query.course_id);

    }

    //获取视频播放页面下方图片
    @HttpGet('/get/coursebgi')
    @ExceptionHandler()
    async getCourseBgimg(){
        const rule = {
            course_bgi_info_content:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.getCourseBgimg(this.ctx.request.query.course_bgi_info_content);
    }

    //课程播放页面获取视频下方课程信息
    @HttpGet('/get/course/details')
    @ExceptionHandler()
    async getDetails(){
        const rule = {
            course_id:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.getDetails(this.ctx.request.query.course_id);


    }

    //查找是否已购买
    @HttpGet('/get/purchased')
    @ExceptionHandler()
    async getPurchased() {
        const rule = {
            id:'int',
            course_id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.getPurchased(this.ctx.request.query.id,this.ctx.request.query.course_id);

    }


    //返回课程信息和价格
    @HttpGet('/priceinfo')
    @ExceptionHandler()
    async getPrice() {
        const rule = {
            course_id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.getPrice(this.ctx.request.query.course_id);

    }

    //中文返回课程信息和价格
    @HttpGet('/priceinfochn')
    @ExceptionHandler()
    async getPricechn() {
        const rule = {
            course_id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.getPricechn(this.ctx.request.query.course_id);

    }

    //用户购买课程
    @HttpPost('/purchase')
    @ExceptionHandler()
    async purchase() {
        const rule = {
            id:'int',
            course_id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.course.main.purchase(this.ctx.request.query.id,this.ctx.request.query.course_id);


    }

    //用户提交观看记录
    @HttpPost('/upload/record')
    @ExceptionHandler()
    async uploadRecord() {
        const rule = {
            id:'int',
            course_id:'int',
            course_section_id: 'int',
            status:'int'
        };
        this.validate(rule,this.ctx.request.body);
        this.result.data = await this.service.china.pc.course.main.uploadRecord(this.ctx.request.body);

    }

    //购买页面返回推荐课程信息
    @HttpGet('/recommend')
    @ExceptionHandler()
    async recommend() {
        this.result.data = await this.service.china.pc.course.main.recommend();
    }
}

module.exports = CourseCtrl;
