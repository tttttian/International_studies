const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/vr')
class VrController extends Controller{

    //VR图获取信息
    @HttpGet('/get/vrinfo')
    @ExceptionHandler()
    async getVrInfo(){
        this.result.data = await this.service.china.pc.vr.main.getVrInfo();

    }
}

module.exports = VrController;
