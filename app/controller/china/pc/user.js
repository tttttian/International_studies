const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/user')
class UserController extends Controller{

    @HttpPost('/register')
    @ExceptionHandler()
    async register() {
        const rule = {
            phone:'string',
            pwd:'string'
        };
        this.validate(rule,this.ctx.request.body);
        this.result.data = await this.service.china.pc.user.main.register(this.ctx.request.body.phone,this.ctx.request.body.pwd);
        return "注册成功"
    }

    //登录
    @HttpPost('/login')
    @ExceptionHandler()
    async login() {
        const rule = {
            phone:'string',
            pwd:'string',
            remember:'int'
        };
        this.validate(rule,this.ctx.request.body);
        this.result.data = await this.service.china.pc.user.main.login(this.ctx.request.body);

    }

    //退出登录
    @HttpPost('logout')
    @ExceptionHandler()
    async logout() {
        await this.service.china.pc.user.main.logout();
        return '退出成功'
    }
}

module.exports = UserController;
