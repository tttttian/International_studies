const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/shop')
class ShopController extends Controller{

    //上方背景图
    @HttpGet('/get/bgimg')
    @ExceptionHandler()
    async getBgimg() {
        const rule = {
            classify: 'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.home.main.getBgimg(this.ctx.request.query.classify);
        return '查询成功'
    }

    //搜索商品
    @HttpGet('/search/goods')
    @ExceptionHandler()
    async searchGoods() {
        const rule = {
            name:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.shop.main.searchGoods(this.ctx.request.query.name);
    }

    //获取商品
    @HttpGet('/get/goods')
    @ExceptionHandler()
    async getGoods() {
        this.result.data = await this.service.china.pc.shop.main.getGoods();
    }

}
module.exports = ShopController;
