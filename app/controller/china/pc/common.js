const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/common')
class CommonController extends Controller{

    //获取所有课程
    @HttpGet('/get/course')
    @ExceptionHandler()
    async getCourse() {
        this.result.data = await this.service.china.pc.common.main.getCourse();
    }

    //获取所有课程小节
    @HttpGet('/get/course_section')
    @ExceptionHandler()
    async getCourseSection() {
        const rule = {
            course_id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.common.main.getCourseSection(this.ctx.request.query.course_id);
    }
}

module.exports = CommonController;
