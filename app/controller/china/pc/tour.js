const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/tour')
class TourController extends Controller{

    //上方背景图
    @HttpGet('/get/bgimg')
    @ExceptionHandler()
    async getBgimg() {
        const rule = {
            classify: 'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.home.main.getBgimg(this.ctx.request.query.classify);
        return '查询成功'
    }

    //获取课程信息
    @HttpGet('/get/courseinfo')
    @ExceptionHandler()
    async getTourCourse() {
        this.result.data = await this.service.china.pc.tour.main.getTourCourse();
    }

    //获取课程信息（中文）
    @HttpGet('/get/courseinfochn')
    @ExceptionHandler()
    async getTourCoursecn() {
        this.result.data = await this.service.china.pc.tour.main.getTourCoursecn();
    }
}

module.exports = TourController;
