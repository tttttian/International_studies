const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/about')
class AbutController extends Controller{

    //返回上方背景
    @HttpGet('/get/bgimg')
    @ExceptionHandler()
    async getAboutBgi() {
        const rule = {
            classify: 'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.home.main.getBgimg(this.ctx.request.query.classify);
        return '查询成功'
    }

    //学习展示
    @HttpGet('/get/studyshow')
    @ExceptionHandler()
    async getShow() {
        this.result.data = await this.service.china.pc.about.main.getShow();
    }

    //团队信息
    @HttpGet('/get/team')
    @ExceptionHandler()
    async getTeam() {
        this.result.data = await this.service.china.pc.about.main.getTeam();

    }

    //提交表单
    @HttpPost('/contact')
    @ExceptionHandler()
    async contact() {
        const rule = {
          name:'string',
          phone:'string',
          email:'string',
          content:'string'
        };
        this.validate(rule,this.ctx.request.body);
        this.result.data = await this.service.china.pc.about.main.contact(this.ctx.request.body);
    }
}
module.exports = AbutController;
