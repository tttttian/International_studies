const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/personal')
class PersonalCtrl extends Controller {

    //个人中心获取用户信息
    @HttpGet('/userinfo')
    @ExceptionHandler()
    async getUser() {
        const rule = {
            phone:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.personal.main.getUser(this.ctx.request.query.phone);
        return '成功'
    }

    //个人中心获取观看记录
    @HttpGet('/watchingrecord')
    @ExceptionHandler()
    async WatchRecord() {
        const rule = {
            id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.personal.main.WatchRecord(this.ctx.request.query.id);

    }

    //完善资料
    @HttpPost('/update/userinfo')
    @ExceptionHandler()
    async updateInfo() {
        const rule = {
            phone:'string',
            avatar_img: 'string',
            nick_name: 'string',
            name:'string',
            sex: 'int',
            address: 'string',
            signature: 'string'
        };
        this.validate(rule,this.ctx.request.body);
        this.result.data = await this.service,china.pc.personal.main.updateInfo(this.ctx.request.body);
    }

    //获取个人信息
    @HttpGet('/get/userinfo')
    @ExceptionHandler()
    async getUserInfo() {
        const rule = {
            id:'int'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.personal.main.getUserInfo(this.ctx.request.query.id);
    }

    //实名认证
    @HttpPost('/update/cardinfo')
    @ExceptionHandler()
    async updateCard() {
        const rule = {
            phone:'string',
            name:'string',
            card_id_number:'string',
            card_image:'string'
        };
        this.validate(rule,this.ctx.request.body);
        this.result.data = await this.service.china.pc.personal.main.updateCard(this.ctx.request.body);


    }

    //修改手机号
    @HttpPost('/update/phone')
    @ExceptionHandler()
    async updatePhone() {
        const rule = {
            phone:'string',
            new_phone:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.personal.main.updatePhone(this.ctx.request.query.phone,this.ctx.request.query.new_phone);


    }

    //修改密码
    @HttpPost('/update/pwd')
    @ExceptionHandler()
    async updatePwd() {
        const rule = {
            phone:'string',
            pwd:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.personal.main.updatePwd(this.ctx.request.query.phone,this.ctx.request.query.pwd);

    }

    //重置密码
    @HttpPost('/reset/pwd')
    @ExceptionHandler()
    async resetPwd() {
        const rule = {
            phone:'string',
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.personal.main.resetPwd(this.ctx.request.query.phone);

    }


}
module.exports = PersonalCtrl;
