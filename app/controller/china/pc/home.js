const Controller = require('../../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../../plugin/egg-controller-exception-handler/lib');

@Route('/api/china/pc/home')
class HomeCtrl extends Controller{

    //返回首页上方背景图
    @HttpGet('/get/bgimg')
    @ExceptionHandler()
    async getBgimg(){
        const rule = {
            classify: 'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.home.main.getBgimg(this.ctx.request.query.classify);
        return '查询成功'
    }

    //返回用户头像信息
    @HttpGet('/get/userinfo')
    @ExceptionHandler()
    async getUserInfo(){
        const rule = {
            phone:'string'
        };
        this.validate(rule,this.ctx.request.query);
        this.result.data = await this.service.china.pc.home.main.getUserInfo(this.ctx.request.query.phone);

    }

    //返回课程信息
    @HttpGet('/get/courseinfo')
    @ExceptionHandler()
    async getCourseInfo(){
        this.result.data = await this.service.china.pc.home.main.getCourseInfo();

    }

    //返回新闻和活动图片
    @HttpGet('/get/activityinfo')
    @ExceptionHandler()
    async getActivityInfo(){
        this.result.data = await this.service.china.pc.home.main.getActivityInfo();

    }

    //提交表单
    @HttpPost('/application')
    @ExceptionHandler()
    async application(){
        const rule = {
            name:'string',
            phone:'string',
            nation:'string',
            email:'string',
            content:'string'
        };
        this.validate(rule,this.ctx.request.body);
        this.result.data = await this.service.china.pc.home.main.application(this.ctx.request.body);
    }
}

module.exports = HomeCtrl;
