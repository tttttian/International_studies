const Controller = require('../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../plugin/egg-controller-exception-handler/lib');
const path = require('path');

@Route('/api/upload/image')
class ImageCtrl extends Controller{

    //上传单个图片
    @HttpPost('/single')
    @ExceptionHandler()
    async UploadImage() {
        const rule = {
            'files': 'array'
        };
        this.validate(rule, this.ctx.request);
        const file = this.ctx.request.files[0];
        const extname = path.extname(file.filename);
        if(extname !== '.jpg' && extname !== '.jpeg' && extname !== '.png'){
            throw new this.error.InvalidError('文件类型必须为 jpg,jpeg,png中的一种！');
        }
        this.result.data = await this.service.upload.image.singleImage(file);
    }
}

module.exports = ImageCtrl;
