const Controller = require('../../core/controller/ApiController');
const {Route, HttpGet, HttpPost, Middleware, filters} = require('../../../plugin/egg-decorator-router/lib');
const {ExceptionHandler} = require('../../../plugin/egg-controller-exception-handler/lib');
const path = require('path');

@Route('/api/upload/video')
class VideoCtrl extends Controller {

    @HttpPost('/single')
    @ExceptionHandler()
    async UploadVideo() {
        const rule = {
            'files': 'array'
        };
        this.validate(rule, this.ctx.request);
        const file = this.ctx.request.files[0];
        const extname = path.extname(file.filename);
        if (extname !== '.mp4' && extname !== '.MOV' && extname !== '.swf') {
            throw new this.error.InvalidError('文件类型必须为 mp4,MOV其中的一种！');
        }
        this.result.data = await this.service.upload.video.singleVideo(file);
    }
}

module.exports = VideoCtrl;
