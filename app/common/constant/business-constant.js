module.exports = {

    //商品状态
    SHOP_STATUS:{
      ENABLE:1,
      DISABLE:2
    },


    //团队照片状态
    ABOUT_TEAM_STATUS:{
        ENABLE: 1,
        DISABLE: 2
    },

    //课程状态
    COURSE_STATUS:{
        ENABLE:1,
        DISABLE:0
    },

    //已购买课程状态
    COURSE_PURCHASED_STATUS:{
        ENABLE:1,
        DISABLE:2,  //不可看
    }

};
