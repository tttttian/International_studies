const Service = require('../../../../core/service/ApiService');

class TourService extends Service{

    //获取课程信息
    async getTourCourse() {
        const dao = this.service.china.pc.tour.dao;
        return dao.getTourCourse();
    }

    //获取课程信息(中文)
    async getTourCoursecn() {
        const dao = this.service.china.pc.tour.dao;
        return dao.getTourCoursecn();
    }
}

module.exports = TourService;
