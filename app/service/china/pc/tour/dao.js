const Service = require('../../../../core/service/ApiService');

class TourDao extends Service{

    //获取课程信息
    async getTourCourse() {
        const sql = `select course_info.course_id,course_name,course_introduction,recommend,trip_introduction,course_date,course_time,course_original_img,course_img,course_below_img
                            from course_info
                            inner join course_original_img coi on course_info.course_id = coi.course_id;
                            `;
        return this.model.query(sql,{type:this.model.QueryTypes.SELECT});
    }

    //获取课程信息(中文)
    async getTourCoursecn() {
        const sql = `select course_info_cn.course_id,course_name,course_introduction,recommend,trip_introduction,course_date,course_time,course_original_img,course_img,course_below_img
                            from course_info_cn
                            inner join course_original_img coi on course_info_cn.course_id = coi.course_id;
                            `;
        return this.model.query(sql,{type:this.model.QueryTypes.SELECT});
    }
}

module.exports = TourDao;
