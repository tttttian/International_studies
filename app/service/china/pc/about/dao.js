const Service = require('../../../../core/service/ApiService');

class AboutDao extends Service{

    //学习展示
    async getShow() {
        return this.model.AboutStudyShow.findAll({
            attributes:['study_show_img'],
            raw:true
        })
    }

    //团队信息
    async getTeam() {
        return this.model.AboutOurTeam.findAll({
            where:{
              status:this.constant.ABOUT_TEAM_STATUS.ENABLE
            },
            attributes: ['our_team_name','our_team_content','our_team_img'],
            raw:true
        })
    }

    //提交表单
    async contact(contact) {
        await this.model.Contact.create({
            name:contact.name,
            phone:contact.phone,
            email:contact.email,
            content:contact.content
        })
    }
}
module.exports = AboutDao;
