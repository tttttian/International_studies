const Service = require('../../../../core/service/ApiService');

class AboutService extends Service{

    //学习展示
    async getShow() {
        const dao = this.service.china.pc.about.dao;
        return dao.getShow();
    }

    //团队信息
    async getTeam() {
        const dao = this.service.china.pc.about.dao;
        return dao.getTeam();
    }

    //提交表单
    async contact(contact) {
        const dao = this.service.china.pc.about.dao;
        return dao.contact(contact);
    }
}
module.exports = AboutService;
