const Service = require('../../../../core/service/ApiService');

class ShopDao extends Service{

    //搜索商品
    async searchGoods(name) {
        return this.model.ShopGoods.findOne({
            where:{
                name:{
                    $like:`%${name}%`
                }
            },
            attributes:['good_name','good_img','good_price','good_content'],
            raw:true
        })
    }

    //获取商品
    async getGoods() {
        return this.model.ShopGoods.findAll({
            where: {
                status:this.constant.SHOP_STATUS.ENABLE
            },
            attributes:['good_name','good_img','good_price','good_content'],
            raw:true
        })
    }
}

module.exports = ShopDao;
