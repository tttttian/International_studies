const Service = require('../../../../core/service/ApiService');

class ShopService extends Service{

    //搜索商品
    async searchGoods(name) {
        const dao = this.service.china.pc.shop.dao;
        return dao.searchGoods(name);
    }

    //获取商品
    async getGoods() {
        const dao = this.service.china.pc.shop.dao;
        return dao.getGoods();
    }
}

module.exports = ShopService;
