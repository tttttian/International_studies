const Service = require('../../../../core/service/ApiService');

class CourseDao extends Service{

    //获取课程信息
    async getCourseInfo(){
            const sql = `select course_id,course_name,course_original_img
                            from course_info
                            inner join course_original_img coi on course_info.course_id = coi.course_id`;
            return this.model.query(sql,{type:this.model.QueryTypes.SELECT});
    }

    //获取视频
    async getVideo(course_id){
        return this.model.CourseSection.findAll({
            where:{
                course_id:course_id
            },
            attributes:['course_section_id','course_section_name','course_section_video'],
            raw:true
        })
    }

    //中文获取视频
    async getVideochn(course_id){
        return this.model.CourseSectionCn.findAll({
            where:{
                course_id:course_id
            },
            attributes:['course_section_id','course_section_name','course_section_video'],
            raw:true
        })
    }

    //获取视频播放页面下方的图片
    async getCourseBgimg(course_bgi_info_content){
        return this.model.CourseBgiInfo.findOne({
            where: {
                course_bgi_info_content:course_bgi_info_content
            },
            attributes: ['course_bgi_info_content','course_bgi_img_url'],
            raw:true
        })
    }

    //课程播放页面获取视频下方课程信息
    async getDetails(course_id) {
        const sql = `select course_info.course_id,course_name,course_introduction,course_date,course_time,course_original_img
                            from course_info
                            inner join course_original_img coi on course_info.course_id = coi.course_id
                            where course_info.course_id = '${course_id}';`;
        return this.model.query(sql,{type:this.model.QueryTypes.SELECT});

    }

    //查找是否已购买
    async getPurchased(id,course_id) {
        return this.model.PersonalCourseRecord.findOne({
            where:{
                id:id,
                course_id:course_id
            },
            attributes:['course_id','status'],
            raw:true
        })
    }

    //获取价格
    async getPrice(course_id) {
        return this.model.CourseInfo.findOne({
            where:{
                course_id:course_id
            },
            attributes:['course_name','course_img','course_introduction','course_price','course_below_img'],
            raw:true
        })
    }

    //中文获取价格
    async getPricechn(course_id) {
        return this.model.CourseInfoCn.findOne({
            where:{
                course_id:course_id
            },
            attributes:['course_name','course_img','course_introduction','course_price','course_below_img'],
            raw:true
        })
    }

    //根据course_id获取课程小节的id
    async getCourseSection(course_id) {
        return this.model.CourseSection.findAll({
            where:{
                course_id:course_id,
                status: 1
            },
            attributes:['course_id','course_section_id'],
            raw:true
        })
    }

    //用户购买课程
    async purchase(courseSection) {
        await this.model.PersonalCourseRecord.bulkCreate(courseSection)
    }

    //用户提交观看记录
    async uploadRecord(record) {
        await this.model.PersonalCourseRecord.update({
            status:record.status
        },
            {
                where:{
                    id:record.id,
                    course_id:record.course_id,
                    course_section_id:record.course_section_id,
                }
            })
    }

    //购买页面返回推荐课程信息
    async recommend() {
        return this.model.CourseInfo.findAll({
            where:{
                status:this.constant.COURSE_STATUS.ENABLE
            },
            attributes:['course_id','course_name','course_img','recommend'],
            raw:true
        })
    }

}

module.exports = CourseDao;
