const Service = require('../../../../core/service/ApiService');

class CourseSvc extends Service{

    //获取课程信息原始图
    async getOriginalCourse(){
        const dao = this.service.china.pc.course.dao;
        return dao.getCourseInfo();

    }

    //点击视频小节获取视频
    async getVideo(course_id){
        const dao = this.service.china.pc.course.dao;
        return dao.getVideo(course_id)
    }

    //中文点击视频小节获取视频
    async getVideochn(course_id){
        const dao = this.service.china.pc.course.dao;
        return dao.getVideochn(course_id)
    }

    //获取视频播放页面下方的图片
    async getCourseBgimg(course_bgi_info_content){
        const dao = this.service.china.pc.course.dao;
        return dao.getCourseBgimg(course_bgi_info_content);
    }

    //课程播放页面获取视频下方课程信息
    async getDetails(course_id){
        const dao = this.service.china.pc.course.dao;
        return dao.getDetails(course_id);
    }

    //查找是否已购买
    async getPurchased(id,course_id) {
        const dao = this.service.china.pc.course.dao;
        return dao.getPurchased(id,course_id);
    }

    //返回课程信息和价格
    async getPrice(course_id) {
        const dao = this.service.china.pc.course.dao;
        return dao.getPrice(course_id);
    }

    //中文返回课程信息和价格
    async getPricechn(course_id) {
        const dao = this.service.china.pc.course.dao;
        return dao.getPricechn(course_id);
    }


    //用户购买课程
    async purchase(id,course_id) {
        const dao = this.service.china.pc.course.dao;
        let courseSection = await dao.getCourseSection(course_id);
        for(let i=0;i<courseSection.length;i++) {
            courseSection[i].id = id;
            courseSection[i].status = 1;
        }
        await dao.purchase(courseSection);
    }

    //用户提交观看记录
    async uploadRecord(record) {
        const dao = this.service.china.pc.course.dao;
        return dao.uploadRecord(record);
    }

    //购买页面返回推荐课程信息
    async recommend() {
        const dao = this.service.china.pc.course.dao;
        return dao.recommend();
    }
}

module.exports = CourseSvc;
