const Service = require('../../../../core/service/ApiService');

class HomeSvc extends Service{

    //获得背景图
    async getBgimg(classify){
        const dao = this.service.china.pc.home.dao;
        return dao.getBgimgByClassify(classify);
    }

    //获取头像信息
    async getUserInfo(phone){
        const dao = this.service.china.pc.home.dao;
        return dao.getUserInfo(phone);
    }

    //获取课程信息
    async getCourseInfo(){
        const dao = this.service.china.pc.home.dao;
        return dao.getCourseInfo();

    }

    //获取新闻信息
    async getActivityInfo(){
        const dao = this.service.china.pc.home.dao;
        return dao.getActivityInfo();

    }

    //提交表单
    async application(application){
        const dao = this.service.china.pc.home.dao;
        await dao.application(application);
    }

}

module.exports = HomeSvc;
