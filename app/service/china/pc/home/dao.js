const Service = require('../../../../core/service/ApiService');

class HomeDao extends Service {

    //获取背景图
    async getBgimgByClassify(classify){
        return this.model.Backgroundimage.findOne({
            where: {
                classify:classify
            },
            attributes:['classify','bgi_img'],
            raw:true
        })
    }

    //获取头像信息
    async getUserInfo(phone){
        return this.model.UserInfo.findOne({
            where: {
                phone:phone
            },
            attributes:['avatar_img','nick_name'],
            raw: true
        })
    }

    //获取课程信息
    async getCourseInfo(){
        return this.model.CourseInfo.findAll({
            attributes:['course_id','course_name','course_img'],
            raw: true
        })
    }

    //获取新闻信息
    async getActivityInfo(){
        return this.model.NewsActivityImg.findAll({
            attributes:['news_activity_img_id','new_activity_img','news_activity_img_content'],
            raw: true
        })
    }

    //提交表单
    async application(application){
        await this.model.Application.create({
            name:application.name,
            phone:application.phone,
            nation:application.nation,
            email:application.email,
            content:application.content
        })
    }
}

module.exports = HomeDao;
