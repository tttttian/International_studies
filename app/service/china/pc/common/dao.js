const Service = require('../../../../core/service/ApiService');

class CommonDao extends Service{

    //获取所有课程
    async getCourse() {
        return this.model.CourseInfo.findAll({
            where:{
                status:this.constant.COURSE_STATUS.ENABLE
            },
            attributes:['course_id','course_name'],
            raw:true
        })
    }

    //获取所有小节课程
    async getCourseSection(course_id) {
        return this.model.CourseSection.findAll({
            where:{
                course_id:course_id
            },
            attributes:['course_section_name'],
            raw:true
        })
    }
}

module.exports = CommonDao;
