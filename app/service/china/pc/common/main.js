const Service = require('../../../../core/service/ApiService');

class CommonService extends Service{

    //获取所有课程
    async getCourse() {
        const dao = this.service.china.pc.common.dao;
        return dao.getCourse();
    }

    //获取所有课程小节
    async getCourseSection(course_id) {
        const dao = this.service.china.pc.common.dao;
        return dao.getCourseSection(course_id);
    }
}

module.exports = CommonService;
