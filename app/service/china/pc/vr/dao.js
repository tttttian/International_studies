const Service = require('../../../../core/service/ApiService');

class VrDao extends Service{

    //获取VR信息
    async getVrInfo() {
        return this.model.VrImg.findOne({
            attributes: ['course_name','vr_img','vr_img_content'],
            raw:true
        })
    }
}
module.exports = VrDao;
