const Service = require('../../../../core/service/ApiService');

class VrService extends Service{

    //获取VR信息
    async getVrInfo(){
        const dao = this.service.china.pc.vr.dao;
        return dao.getVrInfo();
    }
}
module.exports = VrService;
