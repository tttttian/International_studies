const Service = require('../../../../core/service/ApiService');

class PersonalSvc extends Service{

    //个人中心获取信息
    async getUser(phone) {
        const dao = this.service.china.pc.personal.dao;
        return dao.getUser(phone);
    }

    //个人中心获取观看记录
    async WatchRecord(id) {
        const dao = this.service.china.pc.personal.dao;
        return dao.WatchRecord(id);

    }

    //完善资料
    async updateInfo(user_info) {
        const dao = this.service.china.pc.personal.dao;
        return dao.updateInfo(user_info);

    }

    //个人信息
    async getUserInfo(id) {
        const dao = this.service.china.pc.personal.dao;
        const Info = await dao.getUser(id);
        return {
            avatar_img:Info.avatar_img,
            id:Info.id,
            phone:phone
        }
    }

    //实名认证
    async updateCard(body) {
        const dao = this.service.china.pc.personal.dao;
        await dao.updateCard(body);
    }

    //修改手机号
    async updatePhone(phone,new_phone) {
        const dao = this.service.china.pc.personal.dao;
        await dao.updatePhone(phone,new_phone);
    }

    //修改密码
    async updatePwd(phone,pwd) {
        const dao = this.service.china.pc.personal.dao;
        await dao.updatePhone(phone,pwd);
    }

    //重置密码
    async resetPwd(phone) {
        const dao = this.service.china.pc.personal.dao;
        await dao.resetPhone(phone);
    }


}
module.exports = PersonalSvc;
