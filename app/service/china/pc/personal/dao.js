const Service = require('../../../../core/service/ApiService');

class PersonalDao extends Service{

    //个人中心获取信息
    async getUser(id) {
        const sql = `select avatar_img,avatar_img,id,phone.nick_name,hierarchy_name
                            from course_info
                            inner join hierarchy coi on course_info.hierarchy_id = coi.hierarchy_id
                            where id = '${id}`;
        return this.model.query(sql,{type:this.model.QueryTypes.SELECT});
    }

    //个人中心获取观看记录
    async WatchRecord(id) {
        const sql = `select personal_course_record.course_id,course_name,personal_course_record.course_section_id,course_section_name,personal_course_record.status
                            from personal_course_record
                            inner join course_info ci on personal_course_record.course_id = ci.course_id
                            inner join course_section co on personal_course_record.course_id = co.course_id and personal_course_record.course_section_id = co.course_section_id
                            where id = '${id}';`;
        return this.model.query(sql,{type:this.model.QueryTypes.SELECT});
    }

    //完善资料
    async updateInfo(user_info) {
        await this.model.UserInfo.update({
            where: {
                phone:user_info.phone
            }
        },{
            avatar_img:user_info.avatar_img,
            nick_name:user_info.nick_name,
            name:user_info.name,
            sex:user_info.sex,
            address:user_info.address,
            signature:user_info.signature
        })
    }

    //实名认证
    async updateCard(body) {
        await this.model.UserInfo.update({
            where:{
                phone:body.phone
            }
        },{
            name:body.name,
            card_id_number:body.card_id_number,
            card_image:body.card_image
        })
    }

    //修改手机号
    async updatePhone(phone,new_phone) {
        await this.model.UserInfo.update({
            where:{
                phone:phone
            }
        },{
            phone:new_phone
        })
    }

    //修改密码
    async updatePwd(phone,pwd) {
        await this.model.UserInfo.update({
            where:{
                phone:phone
            }
        },{
            pwd:pwd
        })
    }

    //重置密码
    async resetPwd(phone) {
        await this.model.UserInfo.update({
            where:{
                phone:phone
            }
        },{
            pwd:'666666'
        })
    }


}
module.exports = PersonalDao;
