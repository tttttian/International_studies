const Service = require('../../../../core/service/ApiService');

class UserDao extends Service{

    //注册时根据手机号判断是否已经注册
    async getInfoFromDb(phone) {
        return this.model.UserInfo.findOne({
            where:{
                phone:phone,

            },
            attributes:['id','avatar_img','nick_name','phone','pwd'],
            raw:true
        })
    }

    //注册信息
    async register(phone,pwd) {
        return this.model.UserInfo.create({
            phone:phone,
            pwd:pwd
        })
    }


}


module.exports = UserDao;
