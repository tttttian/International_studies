const Subscription = require('egg').Subscription;

class ResetAgedSvcStatus extends Subscription {
    // 通过 schedule 属性来设置定时任务的执行间隔等配置
    static get schedule() {
        return {
            cron: '0 0 0 * * ?', // 每天凌晨0点执行
            // cron: '*/5 * * * * ?', //每隔5秒
            // immediate: true,
            type: 'worker', //只有一个worker执行
            disable:true   //是否启用
        };
    }

    async subscribe(){
        const ctx =  this.ctx;
        try{
            await ctx.service.internal.main.resetAgedSvcStatus();
        }
        catch (error) {
            ctx.logger.error(`[ResetAgedSvcStatus Schedule] execute failed: \n${error}`)
        }
    }
}
module.exports = ResetAgedSvcStatus;
