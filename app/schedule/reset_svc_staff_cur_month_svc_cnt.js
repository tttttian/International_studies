const Subscription = require('egg').Subscription;

class ResetSvcStaffCurMonthSvcCnt extends Subscription {
    // 通过 schedule 属性来设置定时任务的执行间隔等配置
    static get schedule() {
        return {
            cron: '0 0 0 1 * ?', // 每月1日0点0分0秒执行
            // immediate: true,
            type: 'worker', //只有一个worker执行
            disable:true
        };
    }

    async subscribe(){
        const ctx =  this.ctx;
        try{
            await ctx.service.internal.main.resetSvcStaffCurMonthSvcCnt();
        }
        catch (error) {
            ctx.logger.error(`[ResetSvcStaffCurMonthSvcCnt Schedule] execute failed: \n${error}`)
        }
    }
}
module.exports = ResetSvcStaffCurMonthSvcCnt;
