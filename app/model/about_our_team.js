/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('about_our_team', {
    our_team_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true

    },
    our_team_name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    our_team_content: {
      type: DataTypes.STRING(800),
      allowNull: false
    },
    our_team_img: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'about_our_team'
  });

  Model.associate = function() {

  }

  return Model;
};
