/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('vr_img', {
    vr_img_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true
    },
    course_name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    vr_img: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    vr_img_content: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'vr_img'
  });

  Model.associate = function() {

  }

  return Model;
};
