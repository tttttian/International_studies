/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('course_info', {
    course_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true

    },
    course_name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    course_img: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    course_introduction: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    recommend: {
      type: DataTypes.STRING(500),
      allowNull: false,
      defaultValue:DataTypes.literal("EMPTY STRING")
    },
    trip_introduction: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    course_price: {
      type: "DOUBLE(10,2)",
      allowNull: false
    },
    course_date: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    course_time: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    course_below_img: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'course_info'
  });

  Model.associate = function() {

  }

  return Model;
};
