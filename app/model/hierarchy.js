/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('hierarchy', {
    hierarchy_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true

    },
    hierarchy_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    hierarchy_discount: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'hierarchy'
  });

  Model.associate = function() {

  }

  return Model;
};
