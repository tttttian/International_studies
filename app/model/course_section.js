/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('course_section', {
    course_count_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true

    },
    course_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    course_section_id: {
      type: DataTypes.INTEGER(20),
      allowNull: false
    },
    course_section_name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    course_section_video: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'course_section'
  });

  Model.associate = function() {

  }

  return Model;
};
