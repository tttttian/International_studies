/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('user_info', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true
    },
    nick_name: {
      type: DataTypes.CHAR(50),
      allowNull: false,
      defaultValue: '6'
    },
    avatar_img: {
      type: DataTypes.STRING(200),
      allowNull: false,
      defaultValue:''
    },
    phone: {
      type: DataTypes.CHAR(20),
      allowNull: false,
      defaultValue: '33344446666'
    },
    pwd: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: '7'
    },
    sex: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '0'
    },
    address: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    signature: {
      type: DataTypes.STRING(200),
      allowNull: false,
      defaultValue:''
    },
    card_id_number: {
      type: DataTypes.CHAR(20),
      allowNull: false,
      defaultValue: '666666'
    },
    card_image: {
      type: DataTypes.STRING(200),
      allowNull: false,
      defaultValue: ''
    },
    hierarchy_id: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')

    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')

    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      defaultValue: '1'
    }
  }, {
    tableName: 'user_info'
  });

  Model.associate = function() {

  }

  return Model;
};
