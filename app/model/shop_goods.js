/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('shop_goods', {
    good_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true

    },
    good_name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    good_img: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    good_price: {
      type: "DOUBLE(10,2)",
      allowNull: false
    },
    good_content: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'shop_goods'
  });

  Model.associate = function() {

  }

  return Model;
};
