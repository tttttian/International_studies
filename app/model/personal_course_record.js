/* indent size: 2 */

module.exports = app => {
  const DataTypes = app.Sequelize;

  const Model = app.model.define('personal_course_record', {
    course_record_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement:true

    },
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    course_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    course_section_id: {
      type: DataTypes.INTEGER(10),
      allowNull: false
    },
    ctime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    mtime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.literal('CURRENT_TIMESTAMP')
    },
    status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'personal_course_record'
  });

  Model.associate = function() {

  }

  return Model;
};
