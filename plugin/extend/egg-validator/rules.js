module.exports = (app) =>{

    app.validator.addRule(
        'anyDate',
        (rule, val) => {
            if (new Date(val).toString() === 'Invalid Date')
                return '时间格式不合法';
        },
        false,
        (val)=>{return new Date(val)}
    );

    app.validator.addRule('mobile', (rule, val) => {
        if(!/^1[3456789]\d{9}$/.test(val))
            return val;
    });
};
